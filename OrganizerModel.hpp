/*
	*
	* OrganizerModel.hpp - A model to handle the organizer data: Reminders, todos, events and notes.
	*
*/

#pragma once

#include "Global.hpp"

namespace Organizer {

	typedef struct _rem {
		//Title,Text,Alarm,Notification
		QDateTime dateTime;
		QString title;
		QString text;
		bool alarm;
		bool notify;
	} Reminder;
	typedef QList<Reminder *> Reminders;

	typedef struct _event {
		//Title,Text,Location,Alarm,Notification,Repeat(Never/Daily/Weekly/Monthly/Yearly)
		QDateTime dateTime;
		QString title;
		QString text;
		QString location;
		bool alarm;
		bool notify;
		int repeat;			// 0 = Never; 1 = Daily; 2 = Weekly; 3 = Monthly; 4 = Yearly
	} Event;
	typedef QList<Event *> Events;

	typedef struct _note {
		//Title,Text,Location,Alarm,Notification,Repeat(Never/Daily/Weekly/Monthly/Yearly)
		QDateTime dateTime;
		QString note;
	} Note;
	typedef QList<Note *> Notes;

	typedef struct _todo {
		//Text,Alarm,Notification
		QDateTime dateTime;
		QString text;
		int idx;
		bool done;
	} Todo;
	typedef QList<Todo *> Todos;
};

class OrganizerModel : public QObject {
	Q_OBJECT;

	public:
		static OrganizerModel *instance();

		bool hasReminder( QDate );
		bool hasEvent( QDate );
		bool hasNote( QDate );
		bool hasTodo( QDate );

		void setDone( QDateTime, int, bool );

		Organizer::Reminders reminders( QDate );
		Organizer::Events events( QDate );
		Organizer::Notes notes( QDate );
		Organizer::Todos todos( QDate );

	private:
		QSettings *settReminders;
		QSettings *settEvents;
		QSettings *settNotes;
		QSettings *settTodos;

		QMultiMap<QDateTime, Organizer::Reminder *> rMap;
		QMultiMap<QDateTime, Organizer::Event *> eMap;
		QMultiMap<QDateTime, Organizer::Note *> nMap;
		QMultiMap<QDateTime, Organizer::Todo *> tMap;

		QString dataDirPath;

		void reloadModel();

		OrganizerModel();

		static OrganizerModel *model;

	Q_SIGNALS:
		void modelReloaded();
};
