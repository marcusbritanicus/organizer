/*
	*
	* Orgranizer Plugin Viewer
	*
*/

#include "Global.hpp"
#include "OrganizerWidget.hpp"
// #include "CalendarWidget.hpp"

int main(int argc, char *argv[]) {

	QApplication::setAttribute( Qt::AA_EnableHighDpiScaling );

	QApplication app( argc, argv);

	// Set application info
	app.setOrganizationName( "CoreAction" );
	app.setApplicationName( "OrganizerPlugin" );

	OrganizerWidget *org = new OrganizerWidget();
	org->show();

	// CalendarWidget *cw = new CalendarWidget();
	// cw->show();

	return app.exec();
};
