/*
	*
	* OrganizerWidget.hpp - An Orgranizer for CoreAction.
	*
*/

#include "OrganizerWidget.hpp"

/* Name of the plugin */
OrganizerCalWidget::OrganizerCalWidget( QWidget *parent ) : QCalendarWidget( parent ) {

	setMinimumSize( QSize( 300, 200 ) );

	// No Week Number
	setVerticalHeaderFormat( QCalendarWidget::NoVerticalHeader );

	/* Organizer Model */
	model = OrganizerModel::instance();
	connect( model, &OrganizerModel::modelReloaded, [=]() { repaint(); } );
};

void OrganizerCalWidget::paintCell( QPainter *painter, const QRect &rect, const QDate &date ) const {

	painter->setRenderHints( QPainter::TextAntialiasing );

	/* Current date highlight rectangle */
	painter->save();
	if ( date == QDate::currentDate() ) {
		painter->setPen( QPen( palette().color( QPalette::Highlight ), 2, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin ) );
		painter->setBrush( Qt::NoBrush );

		painter->drawRect( rect.adjusted( 1, 1, -1, -1 ) );

		QPoint tlCorner( rect.topLeft() + QPoint( 1, 1 ) );
		QPainterPath triangle;
		triangle.moveTo( tlCorner );
		triangle.lineTo( tlCorner + QPoint( 10, 0 ) );
		triangle.lineTo( tlCorner + QPoint( 0, 10 ) );
		triangle.lineTo( tlCorner );

		painter->setBrush( palette().color( QPalette::Highlight ) );
		painter->drawPath( triangle );
	}

	else if ( date == selectedDate() ) {
		painter->setPen( QPen( palette().color( QPalette::Highlight ), 1 ) );
		painter->setBrush( Qt::NoBrush );

		painter->drawRect( rect.adjusted( 1, 1, -1, -1 ) );
	}
	painter->restore();

	/* Draw the dates */
	painter->save();
	if ( date.month() == monthShown() ) {
		if ( date.dayOfWeek() >= 6 )
			painter->setPen( Qt::red );

		else
			painter->setPen( palette().color( QPalette::Text ) );
	}

	else
		painter->setPen( Qt::lightGray );

	if ( date == QDate::currentDate() ) {
		QFont cFont( painter->font() );
		cFont.setWeight( QFont::Bold );

		painter->setFont( cFont );
	}

	painter->drawText( rect, Qt::AlignCenter, date.toString( "d" ) );
	painter->restore();

	painter->save();
	/* Calendar marks */
	QPointF brCorner( rect.bottomRight() );
	QPointF trCorner( rect.topRight() );

	double lineWidth = qMax( 5.0, rect.width() * 0.15 );
	double buf = 3.0;
	double x = 3.0;

	/* Reminders */
	if ( model->hasReminder( date ) ) {
		painter->setPen( QPen( QColor( "#A52A2A" ), 3.0, Qt::SolidLine, Qt::SquareCap ) );
		painter->drawLine( brCorner - QPointF( x, 3.0 ), brCorner - QPointF( x + lineWidth, 3 ) );
		x += buf + lineWidth;
	}

	/* Events */
	if ( model->hasEvent( date ) ) {
		painter->setPen( QPen( QColor( "#DAA520" ), 3.0, Qt::SolidLine, Qt::SquareCap ) );
		painter->drawLine( brCorner - QPointF( x, 3.0 ), brCorner - QPointF( x + lineWidth, 3 ) );
		x += buf + lineWidth;
	}

	/* Notes */
	if ( model->hasNote( date ) ) {
		painter->setPen( QPen( QColor( "#800080" ), 3.0, Qt::SolidLine, Qt::SquareCap ) );
		painter->drawLine( brCorner - QPointF( x, 3.0 ), brCorner - QPointF( x + lineWidth, 3 ) );
		x += buf + lineWidth;
	}

 	/* TODOs */
	if ( model->hasTodo( date ) ) {
		painter->setPen( QPen( QColor( "#008080" ), 3.0, Qt::SolidLine, Qt::SquareCap ) );
		painter->drawLine( brCorner - QPoint( x, 3.0 ), brCorner - QPoint( x + lineWidth, 3 ) );
		x += buf + lineWidth;
	}

	painter->restore();
};

OrganizerWidget::OrganizerWidget( QWidget *parent ) : QWidget( parent ) {

	calendar = new OrganizerCalWidget( this );
	calendar->setFocusPolicy( Qt::NoFocus );

	infoWidget = new QWidget();
	infoLyt = new QVBoxLayout();
	infoLyt->setSpacing( 0 );

	infoLabel = new QLabel();

	infoLyt->addWidget( infoLabel );
	infoWidget->setLayout( infoLyt );
	infoWidget->hide();

	QVBoxLayout *lyt = new QVBoxLayout();
	lyt->setContentsMargins( QMargins() );
	lyt->setSpacing( 0 );
	lyt->addWidget( calendar );
	lyt->addWidget( infoWidget );

	setLayout( lyt );

	connect( calendar, &OrganizerCalWidget::clicked, [=]( const QDate &date ) {
		showInfo( date );
	} );

	showInfo( QDate::currentDate() );
};

void OrganizerWidget::showInfo( QDate date ) {

	/* Delete all the todos */
	for( QCheckBox *check: infoWidget->findChildren<QCheckBox *>() )
		delete check;

	OrganizerModel *model = OrganizerModel::instance();

	QString info;

	/* Reminders */
	if ( model->hasReminder( date ) ) {
		Organizer::Reminder *rem = model->reminders( date ).at( 0 );

		info += QString( "<b>Remember: %1</b><p>%2</p>" ).arg( rem->title ).arg( rem->text );
		infoLabel->setText( info );
	}

	/* Events */
	if ( model->hasEvent( date ) ) {
		Organizer::Event *event = model->events( date ).at( 0 );

		info += QString( "<b>Events: %1 at %3</b><p>%2</p>" ).arg( event->title ).arg( event->text ).arg( event->location );
		infoLabel->setText( info );
	}

	/* Notes */
	if ( model->hasNote( date ) ) {
		info += QString( "<b>Notes:</b><br>" );
		for ( int i = 0; i < model->notes( date ).count(); i++ ) {
			Organizer::Note *note = model->notes( date ).at( i );

			info += QString( "%1<br>" ).arg( note->note );
		}
		infoLabel->setText( info );

		info += "<br>";
	}

	/* TODOs */
	if ( model->hasTodo( date ) ) {
		info += QString( "<b>To Do:</b><br>" );
		for( int i = 0; i < model->todos( date ).count(); i++ ) {
			Organizer::Todo *todo = model->todos( date ).at( i );

			QCheckBox *check = new QCheckBox( todo->text );
			check->setChecked( todo->done );
			infoLyt->addWidget( check );

			connect(
				check, &QCheckBox::clicked, [=]() {
					model->setDone( todo->dateTime, todo->idx, check->isChecked() );
				}
			);
		}

		infoLabel->setText( info );
	}

	if ( info.count() )
		infoWidget->show();

	/* Hide the info label if there is no info */
	else
		infoWidget->hide();
};
