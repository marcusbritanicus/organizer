/*
	*
	* OrganizerModel.hpp - A model to handle the organizer data: Reminders, todos, events and notes.
	*
*/

#include "OrganizerModel.hpp"

OrganizerModel *OrganizerModel::model = nullptr;

OrganizerModel* OrganizerModel::instance() {

	if ( not model )
		model = new OrganizerModel();

	return model;
}

OrganizerModel::OrganizerModel() : QObject() {

	dataDirPath = QStandardPaths::writableLocation( QStandardPaths::ConfigLocation ) + "/coreapps/" + "OrganizerPlugin/";
	QDir dataDir( dataDirPath );
	if ( not dataDir.exists() )
		dataDir.mkpath( "." );

	settReminders = new QSettings( dataDirPath + "Reminders.conf", QSettings::IniFormat );			// Brown:     #A52A2A
	settEvents = new QSettings( dataDirPath + "Events.conf", QSettings::IniFormat );				// GoldenRod: #DAA520
	settNotes = new QSettings( dataDirPath + "Notes.conf", QSettings::IniFormat );					// Purple:    #800080
	settTodos = new QSettings( dataDirPath + "TODOs.conf", QSettings::IniFormat );					// Teal:      #008080

	QFileSystemWatcher *watcher = new QFileSystemWatcher( this );
	watcher->addPath( settReminders->fileName() );
	watcher->addPath( settEvents->fileName() );
	watcher->addPath( settNotes->fileName() );
	watcher->addPath( settTodos->fileName() );

	connect( watcher, &QFileSystemWatcher::fileChanged, this, &OrganizerModel::reloadModel );

	reloadModel();
};

bool OrganizerModel::hasReminder( QDate date ) {

	Q_FOREACH( QDateTime dt, rMap.uniqueKeys() ) {
		if ( dt.date() == date )
			return true;
	}

	return false;
};

bool OrganizerModel::hasEvent( QDate date ) {

	Q_FOREACH( QDateTime dt, eMap.uniqueKeys() ) {
		// If the date of the event corresponds to selected date
		if ( dt.date() == date )
			return true;
	}

	return false;
};

bool OrganizerModel::hasNote( QDate date ) {

	Q_FOREACH( QDateTime dt, nMap.uniqueKeys() ) {
		if ( dt.date() == date )
			return true;
	}

	return false;
};

bool OrganizerModel::hasTodo( QDate date ) {

	Q_FOREACH( QDateTime dt, tMap.uniqueKeys() ) {
		if ( dt.date() == date )
			return true;
	}

	return false;
};

void OrganizerModel::setDone( QDateTime dt, int idx, bool yes ) {

	QString dtStr = dt.toString( "yyyyMMddTHHmmss" );
	QStringList data = settTodos->value( QString( "%1/%2" ).arg( dtStr ).arg( idx ) ).toStringList();
	data[ 1 ] = ( yes ? "true" : "false" );
	settTodos->setValue( QString( "%1/%2" ).arg( dtStr ).arg( idx ), data  );

	settTodos->sync();
};

Organizer::Reminders OrganizerModel::reminders( QDate date ) {

	Organizer::Reminders rems;

	Q_FOREACH( QDateTime dt, rMap.uniqueKeys() ) {
		if ( dt.date() == date )
			rems << rMap.values( dt );
	}

	return rems;
};

Organizer::Events OrganizerModel::events( QDate date ) {

	Organizer::Events events;

	Q_FOREACH( QDateTime dt, eMap.uniqueKeys() ) {
		if ( dt.date() == date )
			events << eMap.values( dt );
	}

	return events;
};

Organizer::Notes OrganizerModel::notes( QDate date ) {

	Organizer::Notes notes;

	Q_FOREACH( QDateTime dt, nMap.uniqueKeys() ) {
		if ( dt.date() == date )
			notes << nMap.values( dt );
	}

	return notes;
};

Organizer::Todos OrganizerModel::todos( QDate date ) {

	Organizer::Todos todos;

	Q_FOREACH( QDateTime dt, tMap.uniqueKeys() ) {
		if ( dt.date() == date )
			todos << tMap.values( dt );
	}

	return todos;
};

void OrganizerModel::reloadModel() {

	settReminders->sync();
	settEvents->sync();
	settNotes->sync();
	settTodos->sync();

	rMap.clear();
	eMap.clear();
	nMap.clear();
	tMap.clear();

	/* Reminders */
	Q_FOREACH( QString date, settReminders->value( "Dates" ).toStringList() ) {

		/* Each date-time is a group; open it */
		settReminders->beginGroup( date );

		/* Get the QDateTime object for the date-time */
		QDateTime dt = QDateTime::fromString( date, "yyyyMMddTHHmmss" );

		/* Iterate through multiple entries */
		Q_FOREACH( QString key, settReminders->childKeys() ) {

			/* Get the data */
			QStringList data = settReminders->value( key ).toStringList();

			Organizer::Reminder *rem = new Organizer::Reminder();
			rem->dateTime = dt;
			rem->title = data[ 0 ];
			rem->text = data[ 1 ];
			rem->alarm = ( data[ 2 ] == "false" ? false : true );
			rem->notify = ( data[ 3 ] == "false" ? false : true );

			/* Add it to the map */
			rMap.insert( dt, rem );
		}

		/* Close the group */
		settReminders->endGroup();
	}

	/* Events */
	Q_FOREACH( QString date, settEvents->value( "Dates" ).toStringList() ) {

		/* Each date-time is a group; open it */
		settEvents->beginGroup( date );

		/* Get the QDateTime object for the date-time */
		QDateTime dt = QDateTime::fromString( date, "yyyyMMddTHHmmss" );

		/* Iterate through multiple entries */
		Q_FOREACH( QString key, settEvents->childKeys() ) {

			/* Get the data */
			QStringList data = settEvents->value( key ).toStringList();

			Organizer::Event *event = new Organizer::Event();
			event->dateTime = dt;
			event->title = data[ 0 ];
			event->text = data[ 1 ];
			event->location = data[ 2 ];
			event->alarm = ( data[ 3 ] == "false" ? false : true );
			event->notify = ( data[ 4 ] == "false" ? false : true );
			event->repeat = data[ 5 ].toInt();

			/* Add it to the map */
			eMap.insert( dt, event );
		}

		/* Close the group */
		settEvents->endGroup();
	}

	/* Notes */
	Q_FOREACH( QString date, settNotes->value( "Dates" ).toStringList() ) {

		/* Each date-time is a group; open it */
		settNotes->beginGroup( date );

		/* Get the QDateTime object for the date-time */
		QDateTime dt = QDateTime::fromString( date, "yyyyMMddTHHmmss" );

		Q_FOREACH( QString key, settNotes->childKeys() ) {

			QString data = settNotes->value( key ).toString();

			Organizer::Note *note = new Organizer::Note();

			/* Get the data */
			note->dateTime = dt;
			note->note = data;

			/* Add it to the map */
			nMap.insert( dt, note );
		}

		/* Close the group */
		settNotes->endGroup();
	}

	/* TODOs */
	Q_FOREACH( QString date, settTodos->value( "Dates" ).toStringList() ) {

		/* Each date-time is a group; open it */
		settTodos->beginGroup( date );

		/* Get the QDateTime object for the date-time */
		QDateTime dt = QDateTime::fromString( date, "yyyyMMddTHHmmss" );

		/* Iterate through multiple entries */
		int i = 1;
		Q_FOREACH( QString key, settTodos->childKeys() ) {

			/* Get the data */
			QStringList data = settTodos->value( key ).toStringList();

			Organizer::Todo *event = new Organizer::Todo();
			event->dateTime = dt;
			event->text = data[ 0 ];
			event->idx = i;
			event->done = ( data[ 1 ] == "false" ? false : true );

			/* Add it to the map */
			tMap.insert( dt, event );
			i++;
		}

		/* Close the group */
		settTodos->endGroup();
	}

	emit modelReloaded();
};
