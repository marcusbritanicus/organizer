/*
	*
	* OrganizerPlugin.hpp - An Orgranizer plugin for CoreAction.
	*
*/

#pragma once

#include <cprime/cplugininterface.h>
#include <cprime/cvariables.h>
#include "Organizer.hpp"

#include <QtPlugin>

class OrganizerPlugin : public QObject, plugininterface {
	Q_OBJECT

	Q_PLUGIN_METADATA( IID COREACTION_PLUGININTERFACE )
	Q_INTERFACES( plugininterface )

	public:
		/* Name of the plugin */
		QString name();

		/* The plugin version */
		QString version();

		/* The Widget */
		QWidget* widget( QWidget * );
};
