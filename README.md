# Organizer Plugin for CoreAction
## A light-weight plugin to manage Events, Reminders, and ToDos

[CoreAction](https://gitlab.com/cubocore/coreaction), a part of [CoreApps](https://cubocore.gitlab.io/) is a gadget bar that holds some very useful and nifty widgets
like system loads, calendar, calculator, notes, etc. Organizer plugin is an all-in-one alternative for the default Calendar and Notes Plugins, with extra functionality
for adding reminders, events, todos, along with notes.

**Note:** Organizer plugin is not officially a part of CoreApps. The plugin is built using the plugin api detailed in cprime/cplugininterface.h file.

Reminders, events, notes and TODOs are saved in the following format

Reminders:

	[GENERAL]
	Dates = yyyyMMddTHHmmss1, yyyyMMddTHHmmss2, yyyyMMddTHHmmss3, ...

	[yyyyMMddTHHmmss1]
	1=Title,Text,Alarm,Notification
	2=Title,Text,Alarm,Notification

	[yyyyMMddTHHmmss2]
	1=Title,Text,Alarm,Notification
	2=Title,Text,Alarm,Notification

Events:

	[GENERAL]
	Dates = yyyyMMddTHHmmss1, yyyyMMddTHHmmss2, yyyyMMddTHHmmss3, ...

	[yyyyMMddTHHmmss1]
	1=Title,Text,Location,Alarm,Notification,Repeat(Never/Daily/Weekly/Monthly/Yearly)
	2=Title,Text,Location,Alarm,Notification,Repeat(Never/Daily/Weekly/Monthly/Yearly)
	..
	..

Notes:

	[General]
	Dates = yyyyMMddTHHmmss1, yyyyMMddTHHmmss2, yyyyMMddTHHmmss3, ...

	[yyyyMMddTHHmmss1]
	1=Note1
	2=Note2
	3=Note3
	..
	..

TODOs:

	[GENERAL]
	Dates = yyyyMMddTHHmmss1, yyyyMMddTHHmmss2, yyyyMMddTHHmmss3, ...

	[yyyyMMddTHHmmss1]
	1=Item1,Done1
	2=Item2,Done2
	3=Item3,Done3
	4=Item4,Done4
	5=Item5,Done5
	..
	..

**NOTE:** This project is work in progress. Currently, adding reminders/events/notes/todos is not yet implemented. Support for it will be added soon.
