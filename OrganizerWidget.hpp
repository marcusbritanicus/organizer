/*
	*
	* OrganizerWidget.hpp - An Orgranizer for CoreAction.
	*
*/

#pragma once

#include <cprime/cplugininterface.h>
#include "Global.hpp"
#include "OrganizerModel.hpp"

class OrganizerCalWidget : public QCalendarWidget {
	Q_OBJECT

	public:
		OrganizerCalWidget( QWidget *parent = nullptr );

	private:
		OrganizerModel *model;

	protected:
		void paintCell( QPainter *painter, const QRect &rect, const QDate &date ) const override;
};

class OrganizerWidget: public QWidget {
	Q_OBJECT

	public:
		OrganizerWidget( QWidget *parent = nullptr );

	private:
		OrganizerCalWidget *calendar;
		QLabel *infoLabel;

		QWidget *infoWidget;
		QVBoxLayout *infoLyt;

		void showInfo( QDate );
};
