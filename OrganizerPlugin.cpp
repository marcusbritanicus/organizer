/*
	*
	* OrganizerPlugin.hpp - An Orgranizer plugin for CoreAction.
	*
*/

#include "OrganizerPlugin.hpp"

/* Name of the plugin */
QString OrganizerPlugin::name() {

	return "Organizer";
};

/* The plugin version */
QString OrganizerPlugin::version() {

    return "1.0.0";
};

/* The Widget hooks for menus/toolbars */
QWidget* OrganizerPlugin::widget( QWidget *parent ) {

	return new OrganizerWidget( parent );
};
